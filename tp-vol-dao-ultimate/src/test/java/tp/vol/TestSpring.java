package sopra.vol.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import sopra.vol.Civilite;
import sopra.vol.Client;
import sopra.vol.ClientParticulier;
import sopra.vol.dao.IClientRepository;

public class TestSpring {

	public class JUnitNewAgeTest {

	}

	private static ClassPathXmlApplicationContext context = null;

	@Test
	public void client() {
			
			IClientRepository clientRepo = context.getBean(IClientRepository.class);
			
			ClientParticulier client1 = new ClientParticulier  ();
			
			client1 = clientRepo.save(client1);
			
			int startSize = clientRepo.findAll().size();
			
			client1.setNom("Ortolan");
			client1.setPrenom("Marc");
			
			client1.setCivilite(Civilite.M);
			client1.setTelephone("0526498734");
			
			client1 = clientRepo.save(client1);
			
			Optional<Client> optClient1 = clientRepo.findById(client1.getId());
			
			client1 = (ClientParticulier) optClient1.get();
			
			Assert.assertEquals("Ortolan", client1.getNom());
			
			
			
			
		}

}