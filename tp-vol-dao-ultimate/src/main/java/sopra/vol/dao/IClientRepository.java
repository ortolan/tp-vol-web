package sopra.vol.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sopra.vol.Client;
import sopra.vol.ClientParticulier;
import sopra.vol.ClientPro;

public interface IClientRepository extends JpaRepository<Client, Long>{
	

	@Query("select c from ClientParticulier")
	List<ClientParticulier> findAllClientParticulier();
	
	
	@Query("select c from ClientPro")
	List<ClientPro> findAllClientPro();
}
