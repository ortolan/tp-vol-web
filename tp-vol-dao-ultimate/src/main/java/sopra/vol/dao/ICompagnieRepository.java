package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Compagnie;

public interface ICompagnieRepository extends JpaRepository<Compagnie, Long>{
	
}
