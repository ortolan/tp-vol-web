package sopra.vol.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import sopra.vol.Adresse;
import sopra.vol.Aeroport;
import sopra.vol.Civilite;
import sopra.vol.ClientParticulier;
import sopra.vol.Compagnie;
import sopra.vol.MoyenPaiement;
import sopra.vol.Passager;
import sopra.vol.Reservation;
import sopra.vol.Singleton;
import sopra.vol.TypePieceIdentite;
import sopra.vol.Ville;
import sopra.vol.Vol;
import sopra.vol.Voyage;
import sopra.vol.VoyageVol;
import sopra.vol.dao.IAeroportRepository;
import sopra.vol.dao.IClientRepository;
import sopra.vol.dao.ICompagnieRepository;
import sopra.vol.dao.IPassagerDao;
import sopra.vol.dao.IReservationRepository;
import sopra.vol.dao.IVilleRepository;
import sopra.vol.dao.IVolRepository;
import sopra.vol.dao.IVoyageRepository;
import sopra.vol.dao.IVoyageVolRepository;

public class Test {

	public static void main(String[] args) {
		IClientRepository clientDao = Singleton.getInstance().getClientDao();
		IPassagerDao passagerDao = Singleton.getInstance().getPassagerDao();
		IReservationRepository reservationDao = Singleton.getInstance().getReservationDao();
		IVoyageRepository voyageDao = Singleton.getInstance().getVoyageDao();
		ICompagnieRepository compagnieDao = Singleton.getInstance().getCompagnieDao();
		IVoyageVolRepository voyageVolDao = Singleton.getInstance().getVoyagevolDao();
		IVilleRepository villeDao = Singleton.getInstance().getVilleDao();
		IAeroportRepository aeroportDao = Singleton.getInstance().getAeroportDao();
		IVolRepository volDao = Singleton.getInstance().getVolDao();
		
		Adresse monAdresse = new Adresse();
		monAdresse.setVoie("18 bis rue marcelien jourdan");
		monAdresse.setCodePostal("33200");
		monAdresse.setVille("Bordeaux");
		monAdresse.setPays("FRANCE");
		
		ClientParticulier monClient = new ClientParticulier();
		monClient.setNom("Bruel");
		monClient.setPrenom("Patrick");
		monClient.setCivilite(Civilite.M);
		monClient.setMail("saphycamille@gmail.com");
		monClient.setMoyenPaiement(MoyenPaiement.CB);
		monClient = (ClientParticulier) clientDao.save(monClient);
		monClient.setPrincipale(monAdresse);
		monClient.setFacturation(monAdresse);
		
		monClient = (ClientParticulier) clientDao.save(monClient);
		
		Passager monPassager = new Passager();
		monPassager.setNom("Bruel");
		monPassager.setPrenom("Patrick");
		monPassager.setCivilite(Civilite.M);
		SimpleDateFormat spd = new SimpleDateFormat("dd-MM-yyyy");
		try {
			monPassager.setDateValiditePI(spd.parse("04-01-2020"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			monPassager.setDtNaissance(spd.parse("04-01-1978"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		monPassager.setMail("saphycamille@gmail.com");
		monPassager.setNationalite("Française");
		monPassager.setPrincipale(monAdresse);
		monPassager.setTelephone("0617227456");
		monPassager.setTypePI(TypePieceIdentite.PASSEPORT);
		monPassager.setNumIdentite("zad2312154415313");
		
		monPassager = passagerDao.save(monPassager);
		
		Reservation maReservation = new Reservation();
		maReservation.setClient(monClient);
		try {
			maReservation.setDateReservation(spd.parse("04-01-2019"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		maReservation.setNumero("ihf2156125");
		maReservation.setPassager(monPassager);
		maReservation.setStatut(true);
		maReservation.setTarif(599.99f);
		maReservation.setTauxTVA(0.2f);
		
		maReservation= reservationDao.save(maReservation);
		
		monPassager.getReservations().add(maReservation);	
		
		monPassager = passagerDao.save(monPassager);
		
		Voyage monVoyage = new Voyage();
		monVoyage.getReservations().add(maReservation);

		monVoyage = voyageDao.save(monVoyage);
		
		maReservation.setVoyage(monVoyage);
		
		maReservation= reservationDao.save(maReservation);

		Compagnie maCompagnie = new Compagnie();
		maCompagnie.setNomCompagnie("Air France");

		maCompagnie = compagnieDao.save(maCompagnie);
		
		VoyageVol monVoyageVol = new VoyageVol();
		monVoyageVol.setOrdre(1);
		monVoyageVol.setVoyage(monVoyage);
		
		monVoyageVol = voyageVolDao.save(monVoyageVol);
		
		
		monVoyage.getVols().add(monVoyageVol);
		monVoyage = voyageDao.save(monVoyage);

	    List<Ville> villes = new ArrayList <Ville>();
        Ville bdx = new Ville();
        bdx.setNom("Bordeaux");
        villes.add(bdx);
        bdx = villeDao.save(bdx);
    
        Ville tlse = new Ville();
        tlse.setNom("Toulouse");
        villes.add(tlse);
        tlse = villeDao.save(tlse);
        
        Ville amst = new Ville();
        amst.setNom("Amsterdam");
        villes.add(amst);
        amst = villeDao.save(amst);
        
        
    List<Aeroport> aeroports = new ArrayList<Aeroport>();
        Aeroport bx = new Aeroport ();
        bx.setCode("A33");
        bx.getVilles().add(bdx);
       aeroports.add(bx);
       bx = aeroportDao.save(bx);    
       
       Aeroport tse = new Aeroport ();
       tse.setCode("B31");
       tse.getVilles().add(tlse);
       aeroports.add(tse);
       tse = aeroportDao.save(tse);    
       
       Aeroport ams = new Aeroport ();
       ams.setCode("A78");
       ams.getVilles().add(amst);
       aeroports.add(ams);
       ams = aeroportDao.save(ams);
       
       SimpleDateFormat depart1 = new SimpleDateFormat("dd,mm,yyyy");
       SimpleDateFormat arrivee1 = new SimpleDateFormat("dd,mm,yyyy");
       SimpleDateFormat depart2 = new SimpleDateFormat("dd,mm,yyyy");
       SimpleDateFormat arrivee2 = new SimpleDateFormat("dd,mm,yyyy");
       
       
               Vol bxtse = new Vol();
               bxtse.setDepart(bx);
               bxtse.setArrivee(tse);
               bxtse.setNbPlaces(50);
               bxtse.setNumero("BT3331");
               bxtse.setCompagnie(maCompagnie);
               bxtse.getVoyages().add(monVoyageVol);
               
               try {
                   bxtse.setDateDepart(depart1.parse("2019,07,22"));
                   bxtse.setDateArrivee(arrivee1.parse("2019,07,23"));
               } catch (ParseException e) {
                   e.printStackTrace();
               }
               bxtse = volDao.save(bxtse);
               
               Vol tseamst = new Vol();
               tseamst.setDepart(tse);
               tseamst.setArrivee(ams);
               tseamst.setNbPlaces(30);
               tseamst.setNumero("TA31666");
               tseamst.setCompagnie(maCompagnie);
               tseamst.getVoyages().add(monVoyageVol);
               try {
                   tseamst.setDateDepart(depart2.parse("2019,07,24"));
                   tseamst.setDateArrivee(arrivee2.parse("2019,07,25"));
               } catch (ParseException e) {
                   e.printStackTrace();
               }
               
               tseamst = volDao.save(tseamst);

       
		maCompagnie.getVols().add(bxtse);
		maCompagnie.getVols().add(tseamst);
		
		maCompagnie = compagnieDao.save(maCompagnie);
		
		monVoyageVol.setVol(bxtse);
		
		monVoyage = voyageDao.save(monVoyage);
	}}
