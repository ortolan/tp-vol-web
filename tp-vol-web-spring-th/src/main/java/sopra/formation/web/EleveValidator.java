package sopra.formation.web;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import sopra.formation.model.Eleve;

public class EleveValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Eleve.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Eleve eleve = (Eleve) target;

		if (eleve.getPrenom() != null && eleve.getPrenom().length() > 1) {
			int ch = eleve.getPrenom().charAt(0);

			if (ch < 65 || ch > 90) {
				errors.rejectValue("prenom", "prenom.capitalize", "Prénom : 1ère lettre en majuscule");
			}
		}
	}

}
