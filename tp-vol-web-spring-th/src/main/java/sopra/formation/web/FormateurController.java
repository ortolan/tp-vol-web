package sopra.formation.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.formation.dao.IFormateurRepository;
import sopra.formation.model.Civilite;
import sopra.formation.model.Formateur;

@Controller
@RequestMapping("/formateur")
public class FormateurController {
	@Autowired
	private IFormateurRepository formateurRepo;

	public FormateurController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("formateurs", formateurRepo.findAll());

		return "formateur/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("formateur", new Formateur());
		model.addAttribute("civilites", Civilite.values());

		return "formateur/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Formateur> optFormateur = formateurRepo.findById(id);

		if (optFormateur.isPresent()) {
			model.addAttribute("formateur", optFormateur.get());
		}

		model.addAttribute("civilites", Civilite.values());

		return "formateur/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("formateur") @Valid Formateur formateur, BindingResult result) {
		
		if(result.hasErrors()) {
			return "formateur/form";
		}

		formateurRepo.save(formateur);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		formateurRepo.deleteById(id);

		return "redirect:/formateur/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
