package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotEmpty;

@Entity
@DiscriminatorValue("Ville")
public class Ville {

	@Id
	@GeneratedValue
	private Long id;
	
	@Version
	private int version;
	
	@NotEmpty(message = "Le nom est obligatoire")
	private String nom;
	@ManyToMany
	@JoinTable(
			name ="aeroport_ville",
			uniqueConstraints = @UniqueConstraint(columnNames = {"ville_id", "aeroport_id"}),
			joinColumns = @JoinColumn(name="ville_id"),
			inverseJoinColumns = @JoinColumn(name="aeroport_id"))
	private List<Aeroport> aeroports = new ArrayList<Aeroport>();

	public Ville() {
		super();
	}

	public Ville(Long id, String nom) {
		super();
		this.id = id;
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<Aeroport> getAeroports() {
		return aeroports;
	}

	public void setAeroports(List<Aeroport> aeroports) {
		this.aeroports = aeroports;
	}

}
