package sopra.vol;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Vol {
	
	@Version
	private int version;
	
	
	@Id
	@GeneratedValue
	private Long id;
	
	
	private String numero;
	
	@Temporal(TemporalType.DATE)
	@Future
	@NotNull(message = "La date de départ est obligatoire")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateDepart;
	
	@Temporal(TemporalType.DATE)
	@Future
	@NotNull(message = "La date d''arrivée est obligatoire")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateArrivee;
	
	
	private boolean ouvert;
	private int nbPlaces;
	
	@OneToMany(mappedBy = "vol")	
	private List<VoyageVol> voyages = new ArrayList<VoyageVol>();
	@ManyToOne
	@JoinColumn
	private Compagnie compagnie;
	
	
	@ManyToOne
	@JoinColumn
	private Aeroport depart;
	
	
	@ManyToOne
	@JoinColumn
	private Aeroport arrivee;

	public Vol() {
		super();
	}

	public Vol(Long id, String numero, Date dateDepart, Date dateArrivee, boolean ouvert, int nbPlaces) {
		super();
		this.id = id;
		this.numero = numero;
		this.dateDepart = dateDepart;
		this.dateArrivee = dateArrivee;
		this.ouvert = ouvert;
		this.nbPlaces = nbPlaces;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getDateDepart() {
		return dateDepart;
	}

	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}

	public Date getDateArrivee() {
		return dateArrivee;
	}

	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}

	public boolean isOuvert() {
		return ouvert;
	}

	public void setOuvert(boolean ouvert) {
		this.ouvert = ouvert;
	}

	public int getNbPlaces() {
		return nbPlaces;
	}

	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	public List<VoyageVol> getVoyages() {
		return voyages;
	}

	public void setVoyages(List<VoyageVol> voyages) {
		this.voyages = voyages;
	}

	public Compagnie getCompagnie() {
		return compagnie;
	}

	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}

	public Aeroport getDepart() {
		return depart;
	}

	public void setDepart(Aeroport depart) {
		this.depart = depart;
	}

	public Aeroport getArrivee() {
		return arrivee;
	}

	public void setArrivee(Aeroport arrivee) {
		this.arrivee = arrivee;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	
}
